<?php
#  --------------------------------------------------------------- #
#  author: FelipheGomez
#  author URL: http://demedallo.com
#  License: Creative Commons Attribution 3.0 Unported
#  License URL: http://creativecommons.org/licenses/by/3.0/
#  --------------------------------------------------------------- #
require_once("autoload.php");

if(isset($data['accesstoken'])){
	$checkToken = chechear_AccessToken_CCyUser($data['accesstoken']);
	if($checkToken==false){
		$jsonFinal = $errores_API->{'100'}; # Error 100 - El token de acceso es invalido, prueba con otro o genera uno nuevamente
	}else{
		//$permisos_user = $checkToken['permisos_cargos']; # Permisos para la pagina
		
		# Leyendo
		if(isset($data['action']) && $data['action'] == 'view'){
			if(isset($data['id_profile']) && $data['id_profile'] !== '' && $data['id_profile'] > 0){
				$consulta = datosSQL("Select * from ".TBL_PERSONAL." where id='{$data['id_profile']}' ");
				if(isset($consulta->data[0]) && isset($consulta->error) && $consulta->error == false){
					$jsonFinal = $success_API->{'25'}; # Success 25 - Contenido cargado con exito.
					
					
					
					#$accessToken = generate_AccessToken_CCyUser($consulta->data[0]['user'],$consulta->data[0]['cedula']);
					$consulta->data[0]['more'] = json_decode($consulta->data[0]['more']);
					$consulta->data[0]['cargo_name'] = nameCargoById($consulta->data[0]['cargo']);
					$consulta->data[0]['supervisor_name'] = nameJefeById($consulta->data[0]['supervisor']);
					$consulta->data[0]['piloto_name'] = namePilotoById($consulta->data[0]['piloto']);
					$consulta->data[0]['estado_name'] = nameEstadoById($consulta->data[0]['estado']);
					$consulta->data[0]['rol_name'] = nameRolById($consulta->data[0]['rol']);
					$consulta->data[0]['ejecutivo_de_experiencia_name'] = nameJefeById($consulta->data[0]['ejecutivo_de_experiencia']);
					$consulta->data[0]['avatar_url'] = urlImageByAvatar($consulta->data[0]['avatar'],$consulta->data[0]['genero'],$data['accesstoken']);
					$consulta->data[0]["permisos"] = json_decode(permisosByCargos($consulta->data[0]["cargo"]));
					$consulta->data[0]["more"] = json_decode(($consulta->data[0]["more"]));
					
					
					
					#$jsonFinal->data = cargarNamePeopleForUserid($consulta->data[0]['id']);
					$jsonFinal->data = $consulta->data[0];
					
				}
				else{
					$jsonFinal = $errores_API->{'700'}; # Error 700 - Usuario no encontrado, compruebe sus datos e intente nuevamente.
				}
			}
			/*
			# Ver mis indicadores
			if(isset($data['indicators']) && $data['indicators'] == true){
				if(isset($permisos_user->indicators->view) && $permisos_user->indicators->view == true){
					$jsonFinal = $success_API->{'25'}; # Success 25 - Contenido cargado con exito.
					$jsonFinal->data = new stdClass();
					$indicadores = cargarIndicadores($checkToken['id']);
					$jsonFinal->data = organizarKPIs($indicadores,$checkToken);
				}else{
					$jsonFinal = $errores_API->{'200'}; # Error 200 - Permisos insuficientes para realizar está accion.
				}
			}
			else{
				$jsonFinal = $errores_API->{'400'}; # Error 400 - Los campos son invalidos o estan incompletos.
			}*/
		}
		else{
			$jsonFinal = $errores_API->{'400'}; # Error 400 - Los campos son invalidos o estan incompletos.
		}		
	}
}else{
	$jsonFinal = $errores_API->{'110'}; # Error 110 - Falta el token de acceso
};

if(isset($data['action'])){ unset($data['action']); };	
if(isset($data['accesstoken'])){ unset($data['accesstoken']); };	
if(isset($data)){ $jsonFinal->fields = $data; };	
if(isset($permisos_user)){ $jsonFinal->permisos = $permisos_user; };

#FINAL
header('Content-Type: application/json');
echo json_encode($jsonFinal,JSON_PRETTY_PRINT);
return json_encode($jsonFinal,JSON_PRETTY_PRINT);
