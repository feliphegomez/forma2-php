<?php
#  --------------------------------------------------------------- #
#  author: FelipheGomez
#  author URL: http://demedallo.com
#  License: Creative Commons Attribution 3.0 Unported
#  License URL: http://creativecommons.org/licenses/by/3.0/
#  --------------------------------------------------------------- #
require_once('config/defined.php');
require_once('config/functions-global.php');
require_once('config/errors.php');

/* ---------------------------
	- -------------------------------------------- -
	- EXTRAER CAMPOS DE API / FIELDS / GET - POST  -
	- -------------------------------------------- -
---------------------------- */
$jsonFinal = $errores_API->{'50'};
if(isset($_POST) && count($_POST)>0){ $data = ($_POST); }
elseif(isset($_GET) && count($_GET)>0){ $data = ($_GET); }
elseif(isset($_DELETE) && count($_DELETE)>0){ $data = $_DELETE; }
else{ $data = array(); };
