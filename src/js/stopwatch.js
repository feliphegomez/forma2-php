/** INICIO CRONOMETO **/
console.log("Cargando Funciones del Cronometro")
function initCronometro(){
	if(localStorage.cronometro && localStorage.cronometro != undefined && localStorage.cronometro != '' && localStorage.cronometro != ' ' && localStorage.cronometro != 0){
		continuarCronometro();
	}else{
		//console.log("No hay cronometro iniciado.");
	}
}

function detenerCronometro(){
	localStorage.setItem('cronometro', 0);
	localStorage.setItem("cronometro_time", 0);
	localStorage.setItem("cronometro_end", 0);
	min = 0;
	seg = 0;
	
	$(".cronometro-minutos").html(addZero(min));
	$(".cronometro-segundos").html(addZero(seg));
}

function PlayCronometro(minutes){
	var f = new Date();
	localStorage.setItem("cronometro", f);
	localStorage.setItem("cronometro_time", minutes);
	
	texta = f.getFullYear()+'-'+(f.getMonth()+1)+'-'+(f.getDate())+' '+f.getHours()+':'+(f.getMinutes()+minutes)+':'+f.getSeconds();
	
	localStorage.setItem("cronometro_end", new Date(texta));
	continuarCronometro();
}

function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
};

function continuarCronometro(){
	if(localStorage.cronometro && localStorage.cronometro != undefined && localStorage.cronometro != '' && localStorage.cronometro != ' ' && localStorage.cronometro != 0){
		StopTimeCronometro = localStorage.cronometro_time;
		timeInicio = new Date(localStorage.cronometro);
		timeEnd = new Date(localStorage.cronometro_end);
		f_actual = new Date();
		f_actual2 = new Date(f_actual-timeInicio)
		
		//console.log(f_actual2.getMinutes());
		//console.log(f_actual2.getSeconds());
		//console.log(f_actual.getTime());
		
		if(f_actual2.getMinutes() == (StopTimeCronometro-1) && f_actual2.getSeconds()  == 50){
			AlertaDeTiempo1Minuto();
		}else if(f_actual2.getMinutes() >= (StopTimeCronometro) && f_actual2.getSeconds() == 0){
			minutos = 0;
			segundos = 0;
			detenerCronometro();
			PedirMasTiempoCronometro();
		}
		
		$(".cronometro-minutos").html(addZero(f_actual2.getMinutes()));
		$(".cronometro-segundos").html(addZero(f_actual2.getSeconds()));
	
	}
}

function openPopUp(url,options){
	var windowName = 'userConsole'+makeid();
	var popUp = open(url, windowName, options);
	if (popUp == null || typeof(popUp)=='undefined') {  
		$.notify('Por favor deshabilita el bloqueador de ventanas emergentes.','error');
	}else {  
		popUp.focus();
	}
}

function PedirMasTiempoCronometro(){
	var configuracion_ventana = "menubar=no,location=yes,resizable=n0,scrollbars=0,status=0,width=550,height=350";	
	openPopUp(Forma2.options.site_url+'index.php?pageActive=notifications&view=cronometro&action=end',configuracion_ventana);
}

function AlertaDeTiempo1Minuto(){
	var configuracion_ventana = "menubar=no,location=yes,resizable=n0,scrollbars=0,status=0,width=550,height=350";	
	openPopUp(Forma2.options.site_url+'index.php?pageActive=notifications&view=cronometro&action=timeAlerts&value=10',configuracion_ventana);
}

iniciarCronometro = setInterval(function() { initCronometro(); }, 1000);
/** FIN CRONOMETO **/
