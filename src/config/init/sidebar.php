<nav id="sidebar">
	<div class="sidebar-header">
		<!-- .: <?php echo site_name_md; ?> :. -->
		<h3 style="text-align:center;">
			<a href="<?php echo url_site; ?>"><img class="" src="images/logos/Logotrans-350-opt.gif" style="width:100%;" /></a>
		</h3>
	</div>
	<div class="sidebar-profile-box">
		<div class="container-beta">
			<div class="container-beta">
				<div class="forma2-micuenta-avatar-url image-beta-1"></div>
			</div> 
			<div class="overlay-beta">
				<div class="text-beta">					
					<span class="btn-file-image" data-toggle="tooltip" title="Cambiar Imagen">
						<i class="fa fa-camera"></i> <input type="file" accept="image/*" class="imgInp change-my-avatar">
					</span>
				</div>
			</div>
		</div>		
		<a class="forma2-micuenta-link-profile" href="#" title="Visitar mi perfil" data-toggle="tooltip" data-placement="top">
			<span class="forma2-micuenta-nombre">nombre</span>
		</a>
		<br><span class="forma2-micuenta-cargo-name" title="Cargo" data-toggle="tooltip"></span>
		<br><span class="forma2-micuenta-rol-name" title="Rol" data-toggle="tooltip"></span>
		<!-- -->
		<ul class="list-unstyled">
			<li><a class="forma2-micuenta-link-profile" href="#"><span>Mi Perfil</span><i class="lnr lnr-user"></i></a></li>
			<!-- <li><a class="tooltips" href="#"><span>Optiones</span><i class="lnr lnr-cog"></i></a></li> -->
			<!-- <li><a class="tooltips" href="javascript:Forma2.LogOut();"><span>Salir</span><i class="lnr lnr-power-switch"></i></a></li> -->
		</ul>
	</div>
	<ul class="list-unstyled components" id="">
		<p class="forma2-micuenta-piloto-name" title="Piloto" data-toggle="tooltip"></p>
		<div id="menu-sidebar">
		</div>
		
		<!-- 
		<li class="active">
			<a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false">
				<i class="glyphicon glyphicon-home"></i>
				Home
			</a>
			<ul class="collapse list-unstyled" id="homeSubmenu">
				<li><a href="#">Home 1</a></li>
				<li><a href="#">Home 2</a></li>
				<li><a href="#">Home 3</a></li>
			</ul>
		</li>
		<li>
			<a href="#">
				<i class="glyphicon glyphicon-briefcase"></i>
				About
			</a>
		</li>
		<li>
			<a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false">
				<i class="glyphicon glyphicon-duplicate"></i>
				Pages
			</a>
			<ul class="collapse list-unstyled" id="pageSubmenu">
				<li><a href="#">Page 1</a></li>
				<li><a href="#">Page 2</a></li>
				<li><a href="#">Page 3</a></li>
			</ul>
		</li>
		<li>
			<a href="#">
				<i class="glyphicon glyphicon-link"></i>
				Portfolio
			</a>
		</li>
		<li>
			<a href="#">
				<i class="glyphicon glyphicon-paperclip"></i>
				FAQ
			</a>
		</li>
		<li>
			<a href="#">
				<i class="glyphicon glyphicon-send"></i>
				Contact
			</a>
		</li>
		-->
	</ul>
	

	<ul class="list-unstyled CTAs">
		<li>
			<a href="https://web.emtelco.co/ZonaE/" class="download" target="_blank">
				<img width="100%" src="images/logos/logo-emtelco-_cxbpo.png" />
			</a>
		</li>
	</ul>
</nav>
