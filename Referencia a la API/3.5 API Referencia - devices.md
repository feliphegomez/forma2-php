# devices - Dispositivos y Manuales
Esta pagina es utilizada para la lectura de los dispositivos, sus fabricantes, modelos, ver un dispositivo con sus manuales o ver un manual en concreto.

## Leyendo
### Permisos
 - devices.view : El permiso debe estar en el valor true para poder ver los dispositivos o cualquiera de sus componentes.

### Leyendo dispositivo especifico
### Campos
| Nombre de la propiedad | Descripción | Tipo |
| --- | --- | --- |
| id | El ID del dispositivo | int |
| name | Nombre del dispositivo | string |
| image_icon | Id de la imagen del dispositivo | int |
| type | Id del tipo de dispositivo o plataforma | int |
| manufacturer | Id del fabricante del dispositivo | int |
| trash | Indica si el dispositivo se encuentra visible/oculto | int |
| size | Dimenciones de la imagen del dispositivo y las margenes del marco/pantalla. | Object{} |
| manufacturer_name | Nombre del fabricante del dispositivo | string |
| type_name | Nombre de la plataforma o tipo de dispositivo | string |
| image_icon_url | Url de imagen del dispositivo segun token de acceso utilizado | string/url |
| topics | Temas y manuales disponibles para el dispositivo | Object[] |

~~~sh
GET forma2/api
    /v1.0/devices.php?accesstoken={accesstoken}&action=view&id={device-id}
~~~
~~~js
/* Hacer la llamada desde la API */
Forma2.app("POST", "devices", 
{
    "action":"view",
    "id":"{device-id}"
}, function(response){
    console.log(response);
});
~~~


#### Respuesta
~~~json
{
  "error": false,
  "message": "Contenido cargado con exito.",
  "data": {
    "id": "{device-id}",
    "name": "{device-name}",
    "image_icon": "{device-image_icon-id}",
    "type": "{device-type-id}",
    "manufacturer": "{device-manufacturer-id}",
    "trash": "{device-trash}",
    "size": {
      "width": "{device-size-width}",
      "height": "{device-size-height}",
      "screenPositionLeft": "{device-size-screenPositionLeft}",
      "screenPositionTop": "{device-size-screenPositionTop}",
      "screenHeight": "{device-size-screenHeight}",
      "screenWidth": "{device-size-screenWidth}",
      "maxWidth": "{device-size-maxWidth}",
      "maxHeight": "{device-size-maxHeight}"
    },
    "manufacturer_name": "{device-manufacturer-name}",
    "type_name": "{device-type-name}",
    "image_icon_url": "{device-image_icon-url}",
    "topics": [
      {
        "id": "{topic-id}",
        "name": "{topic-name}",
        "device": "{device-id}",
        "trash": "{topic-trash}",
        "items": [
          {
            "id": "{manual-id}",
            "name": "{manual-name}",
            "device": "{device-id}",
            "instructions": [
              {
                "title": "{manual-instructions-title}",
                "steps": [
                  {
                    "id": "{manual-instructions-step}",
                    "text": "{manual-instructions-text}",
                    "points": [
                      {
                        "display": "{manual-instructions-pointer-display}",
                        "displayWidth": "{manual-instructions-pointer-displayWidth}",
                        "displayHeight": "{manual-instructions-pointer-displayHeight}",
                        "pointerSpeed": "{manual-instructions-pointer-pointerSpeed}",
                        "pointerFrames": "{manual-instructions-pointer-pointerFrames}",
                        "pointerWidth": "{manual-instructions-pointer-pointerWidth}",
                        "pointerHeight": "{manual-instructions-pointer-pointerHeight}",
                        "top": "{manual-instructions-pointer-top}",
                        "left": "{manual-instructions-pointer-left}",
                        "orientation": "{manual-instructions-pointer-orientation}",
                        "pointerType": "{manual-instructions-pointer-pointerType}",
                        "pointer": "{manual-instructions-pointer-pointer}",
                        "pointerTop": "{manual-instructions-pointer-pointerTop}",
                        "pointerLeft": "{manual-instructions-pointer-pointerLeft}",
                        "class": "{manual-instructions-pointer-class}"
                      }
                    ]
                  }
                ]
              }
            ],
            "topic": "{topic-id}",
            "trash": "{topic-trash}"
          }
        ]
      }
    ]
  },
  "fields": {
    "id": "{device-id}"
  },
  "permisos": {
    "...": "..."
  }
}
~~~




### Leyendo un manual especifico
### Campos
| Nombre de la propiedad | Descripción | Tipo |
| --- | --- | --- |
| device | Informacion sobre el dispositivo | Objec{} |
| manufacturer | Informacion sobre el fabricante del dispositivo | Object{} |
| manual | Informacion o paso a paso del manual | Object{} |
| topic | Id del tema del manual del dispositivo | int |
| trash | Indica si el manual esta activo/oculto | int |

~~~sh
GET forma2/api
    /v1.0/devices.php?accesstoken={accesstoken}&action=view&device_id={device-id}&vsteps_id={manual-id}
~~~
~~~js
/* Hacer la llamada desde la API */
Forma2.app("POST", "devices", 
{
    "action":"view",
    "device_id":"{device-id}",
    "vsteps_id":"{manual-id}"
}, function(response){
    console.log(response);
});
~~~


#### Respuesta
~~~json
{
  "error": false,
  "message": "Contenido cargado con exito.",
  "data": {
    "device": {
      "id": "{device-id}",
      "name": "{device-name}",
      "image_icon": "{device-image_icon-id}",
      "type": "{device-type-id}",
      "manufacturer": "{device-manufacturer-id}",
      "trash": "{device-trash}",
      "size": {
        "width": "{device-size-width}",
        "height": "{device-size-height}",
        "screenPositionLeft": "{device-size-screenPositionLeft}",
        "screenPositionTop": "{device-size-screenPositionTop}",
        "screenHeight": "{device-size-screenHeight}",
        "screenWidth": "{device-size-screenWidth}",
        "maxWidth": "{device-size-maxWidth}",
        "maxHeight": "{device-size-maxHeight}"
      }
    },
    "manufacturer": {
      "id": "{device-manufacturer-id}",
      "name": "{device-manufacturer-name}",
      "image_icon": "{device-manufacturer-image_icon-id}",
      "type": "{device-manufacturer-type}",
      "trash": "{device-manufacturer-trash}",
      "plataforma_name": "{device-type-name}",
      "plataforma_image": "{device-type-image-id}"
    },
    "manual": {
      "id": "{manual-id}",
      "name": "{manual-name}",
      "device": "{device-id}",
      "instructions": [
        {
          "title": "{manual-instructions-title}",
          "steps": [
            {
              "id": "{manual-instructions-step}",
              "text": "{manual-instructions-text}",
              "points": [
                {
                  "display": "{manual-instructions-pointer-display}",
                  "displayWidth": "{manual-instructions-pointer-displayWidth}",
                  "displayHeight": "{manual-instructions-pointer-displayHeight}",
                  "pointerSpeed": "{manual-instructions-pointer-pointerSpeed}",
                  "pointerFrames": "{manual-instructions-pointer-pointerFrames}",
                  "pointerWidth": "{manual-instructions-pointer-pointerWidth}",
                  "pointerHeight": "{manual-instructions-pointer-pointerHeight}",
                  "top": "{manual-instructions-pointer-top}",
                  "left": "{manual-instructions-pointer-left}",
                  "orientation": "{manual-instructions-pointer-orientation}",
                  "pointerType": "{manual-instructions-pointer-pointerType}",
                  "pointer": "{manual-instructions-pointer-pointer}",
                  "pointerTop": "{manual-instructions-pointer-pointerTop}",
                  "pointerLeft": "{manual-instructions-pointer-pointerLeft}",
                  "class": "{manual-instructions-pointer-class}"
                }
              ]
            }
          ]
        }
      ],
      "topic": "{topic-id}",
      "trash": "{manual-trash}"
    }
  },
  "fields": {
    "vsteps_id": "{manual-id}",
    "device_id": "{device-id}"
  },
  "permisos": {
    "view": true
  }
}
~~~





### Leyendo Plataformas con dispositivos (Arbol)
### Campos Plataformas
| Nombre de la propiedad | Descripción | Tipo |
| --- | --- | --- |
| id | El ID de la platadorma o tipo de dispositivo | int |
| name | Nombre de la plataforma del dispositivo | string |
| image_icon | Id de la imagen de la plataforma | int |
| icon | **Clase**/class del Icono de la plataforma | string |
| trash | Indica si la pregunta/comentario esta activo/oculto | int |
| tree | Listado de dispositivos disponibles según la plataforma. | Object[] |
### Campos Dispositivos
| Nombre de la propiedad | Descripción | Tipo |
| --- | --- | --- |
| id | El ID del dispositivo | int |
| name | Nombre del dispositivo | string |
| image_icon | Id de la imagen del dispositivo | int |
| type | Id del tipo de dispositivo o plataforma | int |
| trash | Indica si la pregunta/comentario esta activo/oculto | int |
~~~sh
GET forma2/api
    /v1.0/devices.php?accesstoken={accesstoken}&action=view&list=true&type=sidebar
~~~
~~~js
/* Hacer la llamada desde la API */
Forma2.app("POST", "devices", 
{
    "action":"view",
    "list":"true",
    "type":"sidebar"
}, function(response){
    console.log(response);
});
~~~

#### Respuesta
~~~json
{
	"error": false,
	"message": "Contenido cargado con exito.",
	"data": [
		{
			"id": "{manufacturer-id}",
			"name": "{manufacturer-name}",
			"image_icon": "{manufacturer-image_icon-id}",
			"icon": "{manufacturer-icon}",
			"trash": "{manufacturer-trash}",
			"tree": [
				{
					"id": "{device-id}",
					"name": "{device-name}",
					"image_icon": "{device-image_icon-id}",
					"type": "{device-type}",
					"trash": "{device-trash}"
				}
			]
		}
	],
	"fields": {
		"list": "true",
		"type": "sidebar"
	},
	"permisos": {
		"...": "..."
	}
}
~~~












### Leyendo dispositivos por fabricante y por plataforma
### Campos
| Nombre de la propiedad | Descripción | Tipo |
| --- | --- | --- |
| id | El ID del dispositivo | int |
| name | Nombre del dispositivo | string |
| image_icon | Id de la imagen del dispositivo | int |
| type | Id del tipo de dispositivo o plataforma | int |
| manufacturer | Id del fabricante del dispositivo | int |
| trash | Indica si el dispositivo se encuentra visible/oculto | int |
| size | Dimenciones de la imagen del dispositivo y las margenes del marco/pantalla. | Object{} |
| image_icon_url | Url de imagen del dispositivo segun token de acceso utilizado | string/url |

~~~sh
GET forma2/api
    /v1.0/devices.php?accesstoken={accesstoken}&action=view&view_devices=true&type={type-id}&manufacturer={manufacturer-id}
~~~
~~~js
/* Hacer la llamada desde la API */
Forma2.app("POST", "devices", 
{
    "action":"view",
    "view_devices":"true",
    "type":"{type-id}",
    "manufacturer":"{manufacturer-id}"
}, function(response){
    console.log(response);
});
~~~

#### Respuesta
~~~json
{
  "error": false,
  "message": "Contenido cargado con exito.",
  "data": [
    {
		"id": "{device-id}",
		"name": "{device-name}",
		"image_icon": "{device-image_icon-id}",
		"type": "{device-type-id}",
		"manufacturer": "{device-manufacturer-id}",
		"trash": "{device-trash}",
		"size": {
			"width": "{device-size-width}",
			"height": "{device-size-height}",
			"screenPositionLeft": "{device-size-screenPositionLeft}",
			"screenPositionTop": "{device-size-screenPositionTop}",
			"screenHeight": "{device-size-screenHeight}",
			"screenWidth": "{device-size-screenWidth}",
			"maxWidth": "{device-size-maxWidth}",
			"maxHeight": "{device-size-maxHeight}"
		},
		"image_icon_url": "{device-image_icon-url}"
    }
  ],
  "fields": {
    "view_devices": "true",
    "type": "{type-id}",
    "manufacturer": "{manufacturer-id}",
	"order": "{order}",
	"page": "{page}",
	"limit": "{limit-page}",
	"offset": "{offset}",
	"page_next": "{next-page}"
  },
  "permisos": {
    "view": true
  }
}
~~~

#### Paginacion
Puedes realizar una exploracion si hay demaciados resultados.
| Campo / Field | Funcionalidad | Tipo | Valor por defecto |
| --- | --- | --- | --- |
| page | # Pagina en la que deseas navegar | int | 1 |
| limit | Limite de resultados por pagina | int | 10 |
| order | Puedes ordenar | string(DESC/ASC) | DESC |

## Creando
La creacion de los dispositivos y los manuales no se encuentra habilitada ya que se encuentra en estado BETA.

## Modificando
La modificacion de los dispositivos y los manuales no se encuentra habilitada ya que se encuentra en estado BETA.

## Eliminando
La eliminacion de los dispositivos y los manuales no se encuentra habilitada ya que se encuentra en estado BETA.






