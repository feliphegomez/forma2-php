# categories - Categorias/Temas de contenido
Esta pagina puede ser utilizada para gestionar las categorias o temas de:
 - Foro [forum]
 - Calendario [calendary]
 - Publicaciones [ecards/articles]


## Leyendo
### Permisos
 - type.categories.view : El permiso debe estar en el valor true para poder ver las categorias de manera correcta, donde "**type**" es igual a:
    - forum: Para las categorias/temas de los foros.
    - calendary: Para las categorias/temas de las capacitaciones.
    - ecards: Para las categorias/temas de las publicaciones tipo eCard.
    - articles: Para las categorias/temas de las publicaciones tipo Articulo.

### Campos
| Nombre de la propiedad | Descripción | Tipo |
| --- | --- | --- |
| id | El ID de la categoria | int |
| name | Nombre de la categoria | string |
| icon | Icono de la categoria | string |
| raiz | Categorias principal o raiz de la categoria vista | int |
| piloto | Id del piloto de la categoria | int |
| type | Tipo de categoria | string |
| view | Indica si la categoria se encuentra visible/oculta | int |
| tree | Subcategorias, contiene la misma informacion dada en esta tabla. (* Solo para vista en arbol ) | Array[] |


### Leyendo categorias en arbol
~~~sh
GET forma2/api
    /v1.0/categories.php?accesstoken={accesstoken}&action=view&list=true&type={category-type}
~~~
~~~js
/* Hacer la llamada desde la API */
Forma2.app("POST", "categories", 
{
    "action":"view",
    "list":"true",
    "type":"{category-type}"
}, function(response){
    console.log(response);
});
~~~

#### Respuesta
~~~json
{
    "error": false,
    "message": "Contenido cargado con exito.",
    "data": [
        {
            "id": "{category-id}",
            "name": "{category-name}",
            "icon": "{category-icon}",
            "raiz": "{category-raiz}",
            "piloto": "{category-piloto}",
            "type": "{category-type}",
            "view": "{category-view}",
            "tree": [
                {
                    "...": "..."
                }
            ]
        }
    ],
    "type": "{category-type}",
    "fields": {
        "list": "true",
        "type": "{category-type}"
    },
    "permisos": {
        "...": "..."
    }
}
~~~


### Leyendo categorias para option list
~~~sh
GET forma2/api
    /v1.0/categories.php?accesstoken={accesstoken}&action=view&option_list=true&type={category-type}
~~~
~~~js
/* Hacer la llamada desde la API */
Forma2.app("POST", "categories", 
{
    "action":"view",
    "option_list":"true",
    "type":"{category-type}"
}, function(response){
    console.log(response);
});
~~~

#### Respuesta
~~~json
{
    "error": false,
    "message": "Contenido cargado con exito.",
    "data": [
        {
            "text": "{category-name}",
            "value": "{category-id}"
        }
    ],
    "fields": {
        "option_list": "true",
        "type": "{category-type}"
    },
    "permisos": {
        "...": "..."
    }
}
~~~

### Leyendo categoria especifica
~~~sh
GET forma2/api
    /v1.0/categories.php?accesstoken={accesstoken}&action=view&id={category-id}
~~~
~~~js
/* Hacer la llamada desde la API */
Forma2.app("POST", "categories", 
{
    "action":"view",
    "ic":"{category-id}"
}, function(response){
    console.log(response);
});
~~~

#### Respuesta
~~~json
{
    "error": false,
    "message": "Contenido cargado con exito.",
    "data": {
        "id": "{category-id}",
        "name": "{category-name}",
        "icon": "{category-icon}",
        "raiz": "{category-raiz}",
        "piloto": "{category-piloto}",
        "type": "{category-type}",
        "view": "{category-view}"
    },
    "fields": {
        "id": "{category-id}"
    },
    "permisos": {
        "...": "..."
    }
}
~~~




## Creando
### Permisos
 - type.categories.create : El permiso debe estar en el valor true para poder ver las categorias de manera correcta, donde "**type**" es igual a:
    - forum: Para las categorias/temas de los foros.
    - calendary: Para las categorias/temas de las capacitaciones.
    - ecards: Para las categorias/temas de las publicaciones tipo eCard.
    - articles: Para las categorias/temas de las publicaciones tipo Articulo.

### Creando una categoria
#### Campos
| Requerido | Campo / Field | Descripción | Tipo de campo | Longitud de campo |
| --- | --- | --- | --- | --- |
| SI | name | Titulo, Nombre o tema de la categoria | string | 250 |
| SI | type | Tipo de categoria (forum,articles,ecards,calendary) | string | 50 |
| SI | raiz | Id de la categoria raiz, si se va a crear una categoria principal este campo debe ir en "**0**" | int | 11 |


~~~sh
GET forma2/api
    /v1.0/categories.php?accesstoken={accesstoken}&action=create&name={category-name}&type={category-type}&raiz={category-raiz}
~~~
~~~js
/* Hacer la llamada desde la API */
Forma2.app("POST", "categories", 
{
    "action":"create",
    "name":"{category-name}",
    "type":"{category-type}",
    "raiz":"{category-raiz}"
}, function(response){
    console.log(response);
});
~~~

#### Respuesta
~~~json
{
    "error": false,
    "message": "Contenido creado con exito.",
    "id": "{category-id}",
    "fields": {
        "name": "{category-name}",
        "type": "{category-type}",
        "raiz": "{category-raiz}"
    },
    "permisos": {
        "...": "..."
    }
}
~~~



## Modificando
### Permisos
 - calendary.edit : El permiso debe estar en el valor true para poder modificar alertas.

### Modificando un calendario especifico
#### Campos
| Requerido | Campo / Field | Descripción | Tipo de campo | Longitud de campo |
| --- | --- | --- | --- | --- |
| SI | id | Id de la categoria | id | 11 |
| SI | name | Titulo, Nombre o tema de la categoria | string | 250 |
| SI | type | Tipo de categoria (forum,articles,ecards,calendary) | string | 50 |
| SI | raiz | Id de la categoria raiz, si se va a crear una categoria principal este campo debe ir en "**0**" | int | 11 |

~~~sh
GET forma2/api
    /v1.0/categories.php?accesstoken={accesstoken}&action=change&id={category-id}&name={category-name}&type={category-type}&raiz={category-raiz}
~~~
~~~js
/* Hacer la llamada desde la API */
Forma2.app("POST", "categories", 
{
    "action":"change",
    "id":"{category-id}",
    "name":"{category-name}",
    "type":"{category-type}",
    "raiz":"{category-raiz}"
}, function(response){
    console.log(response);
});
~~~

#### Respuesta
~~~json
{
    "error": false,
    "message": "Contenido creado con exito.",
    "id": "{category-id}",
    "fields": {
        "name": "{category-name}",
        "type": "{category-type}",
        "raiz": "{category-raiz}",
        "id": "{category-id}"
    },
    "permisos": {
        "...": "..."
    }
}
~~~



## Eliminando
### Permisos
 - type.categories.delete : El permiso debe estar en el valor true para poder eliminar las categorias, donde "**type**" es igual a:
    - forum: Para las categorias/temas de los foros.
    - calendary: Para las categorias/temas de las capacitaciones.
    - ecards: Para las categorias/temas de las publicaciones tipo eCard.
    - articles: Para las categorias/temas de las publicaciones tipo Articulo.

~~~sh
GET forma2/api
    /v1.0/categories.php?accesstoken={accesstoken}&action=delete&id={category-id}
~~~
~~~js
/* Hacer la llamada desde la API */
Forma2.app("POST", "categories", 
{
    "action":"delete",
    "id":"{category-id}"
}, function(response){
    console.log(response);
});
~~~

#### Respuesta
~~~json
{
    "error": false,
    "message": "Contenido eliminado con exito.",
    "fields": {
        "id": "{category-id}"
    },
    "permisos": {
        "...": "..."
    }
}
~~~




